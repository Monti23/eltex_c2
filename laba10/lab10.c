#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

#define KEY 230816
#define MAXNITEMS 15

int n;
int mine = 100;
pthread_t* tid;
struct {
	pthread_mutex_t	mutex;
	int	buff[MAXNITEMS];
	int	nput;
	int	nval;
} shared = { 
	PTHREAD_MUTEX_INITIALIZER
};

void* robokop (void * arg) {
	srand(getpid());
	int id = 0;
	for( int j = 0; j < n; j++) {		
		if( pthread_self() == *(tid+j) ) { id = j + 1; 	break;}
	}
	while(1) {
		sleep(rand() % 5);
		int k = rand()%9 + 1;
		pthread_mutex_lock(&shared.mutex);
		if( mine - k <= 0 ) {
			printf("Unit #%d mined %d, remained %d\n", id, mine, 0);
			printf("Mine is empty\n");
			exit(NULL);
		}			
		mine -= k;			
		printf("Unit #%d mined %d, remained %d\n", id, k, mine);
		pthread_mutex_unlock(&shared.mutex);			
		}	
}


int main (int argc, char** argv) {

	if( argc < 2 ) { printf("Too few argument. Enter %s int", argv[0]);		return -1; }
	if( argc > 2 ) { printf("Too many argument. Enter %s int", argv[0]);	return -1; }
	n = atoi(argv[1]);
	
	//n = 5;
	pthread_t thread_id[n];
	tid = thread_id;
	for(int i = 0; i < n; i++) {		
		if( pthread_create(&thread_id[i], NULL, robokop, NULL) != 0 ) {
			printf("Error: dont create thread %d. Exit\n", i); 
			return -2;
		}
	}
	for(int i = 0; i < n; i++) {
		if( pthread_join(thread_id[i], NULL) != 0 ) {
				printf("Error: dont join thread %d. Exit \n", i); 
				return -2;	
		}		
	}
	exit(0);
}
