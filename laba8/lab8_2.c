/* 
 * File:   main.cpp
 * Author: tomahawk
 *
 * Created on 6 февраля 2018 г., 14:28
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#define KEY 32769

int n, m;

struct mymsgbuf {
    long mtype;
    double mtext;
};

double mat_ozhidanie (void) {
	int array[n], sum = 0;
    double D = 0;
    for (int k = 0; k < n; k++) {
       array[k] = rand() % 10;
       sum += array[k];
    }
    D = (double) sum / (double) n;
    return D;
}

int main(int argc, char** argv) {

    if (argc < 3) {
            printf("Usage: ./hunger_games int int \n");
            exit(-1); 
    }	
	
    n = atoi(argv[2]);                  // кол-во аргументов в массиве
    m = atoi(argv[1]);  				// количество процессов 
    
    pid_t pid[m];
    struct mymsgbuf qbuf;
    int msgqid;
 
    msgqid = msgget(KEY, 0600 | IPC_CREAT);

    for (int i = 0; i < m; i++) {
        pid[i] = fork();
        srand(getpid());

        if (pid[i] == -1) {
            perror("fork failed");
            exit(-1);
        }
        if (pid[i] == 0) { // child procces
			double D = mat_ozhidanie();
            qbuf.mtype = 1;
            qbuf.mtext = D;
            msgsnd(msgqid, &qbuf, sizeof(double), 0); // send D
            exit(2);
        } 
    }
    int stat = 0;
    for(int i = 0; i < m; i++) { // parents procces
		waitpid(pid[i], &stat, 0);
        msgrcv(msgqid, &qbuf, sizeof (qbuf) - sizeof(long), 1, 0); // read msg                    
        printf("Procces %d: %.3f\n", i, qbuf.mtext);
        }
    int rc;
    msgqid = msgget(KEY, 0600 | IPC_EXCL);
    if ((rc = msgctl(msgqid, IPC_RMID, NULL)) < 0) {
        perror(strerror(errno));
        printf("msgctl (return queue) failed, rc=%d \n", rc);
        return -1;
    }
    return 0;
}

