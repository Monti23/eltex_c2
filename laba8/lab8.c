#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>

struct mymsgbuf {
        long mtype;
        double mtext;
};

int main(int argc, char** argv) {
    
	if (argc < 3) {
		printf("Usage: ./hunger_games int int \n");
		exit(-1); 
	}	
	
	int n = atoi(argv[2]);                  // кол-во аргументов в массиве
    int m = atoi(argv[1]);                  // количество процессов
    pid_t pid;
    double read_MO = 0;
    struct mymsgbuf qbuf;
    int msgqid;

    msgqid = msgget(32769, 0600|IPC_CREAT);
        
    for(int i = 0; i < m; i++) {		
		pid = fork();
		srand(getpid());
		
		if(pid == -1) {
			perror("fork failed");
			exit(-1);
		}		
		if( pid == 0 ) {        // child procces
                    int array[n], sum = 0;
                    double D = 0;
                    for(int k = 0; k < n; k++) {
                        array[k] = rand()%10;
                        sum += array[k];
                    }
                    D = (double)sum/(double)n;
                    qbuf.mtype = i;
                    qbuf.mtext = D;                   
                    msgsnd(msgqid, &qbuf, 1, 0);        // send #process and D
                    exit(1);                    
                }
         } 
        int status, stat;      
      for(int i = 0; i < m; i++){                         // parents procces
			status = waitpid(
            msgrcv(msgqid, &read_MO, sizeof(float), i, 0);      // read msg                    
            printf("Procces %d: %.3f", i, read_MO);
      }
	
    int rc;
    
    msgqid = msgget(32769, 0600|IPC_EXCL);
    if ((rc = msgctl(msgqid, IPC_RMID, NULL)) < 0) {
		perror( strerror(errno) );
		printf("msgctl (return queue) failed, rc=%d\n", rc);
		return -1;
	}
    return 0;
}
