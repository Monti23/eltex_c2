/* 
 * File:   main.c
 * Author: tomahawk
 *
 * Created on 9 февраля 2018 г., 14:43
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>
#define KEY 26794

pid_t pid[5];

union semun {
 int val;                  /* значение для SETVAL */
 struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
 unsigned short *array;    /* массивы для GETALL, SETALL */
                           /* часть, особенная для Linux: */
 struct seminfo *__buf;    /* буфер для IPC_INFO */
};


int main(int argc, char* argv[]) {
    /*if(argc < 2) {
            printf("Error: ./warcraft int int \n");
            exit(-1);
    }*/

    //int n = atoi(argv[2]);				// количество юнитов 
    //int V = atoi(argv[1]);				// объем шахты
    int n = 5;
    int *V, m = 100;  
    V = &m;
    
    pid_t pid_p = getpid();     
    
    union semun arg[n];
    int semid = semget(KEY, 1, 0666 | IPC_CREAT);       // создание семафора
    for(int i = 0; i < n; i++) {
		arg[i].val = 0;
    }
    semctl(semid, 0, SETVAL, arg);
    
    struct sembuf lock_res = {0, -1, 0};                //блокировка ресурса
    struct sembuf rel_res = {0, 1, 0};                  //освобождение ресурса
    
    int shmid = shmget(KEY, n, IPC_CREAT | 0666);       // создание разделяемой памяти
 
    int *mine;
    mine = shmat(shmid, 0, 0);
    *mine = *V;
    printf("Объем шахты %d\n", *mine);

    for (int i = 0; i < n; i++) {
        pid[i] = fork();
        srand(getpid());

        if (pid[i] == -1) {
            perror("fork failed\n");
            exit(-1);
        }
        if (pid[i] == 0) { // child
            while(1) {            
            sleep(rand() % 5);
            int k = rand() % 9 + 1;
            arg[i].val = 1;
            semop(semid, arg, 1);
            *V = *mine;
            if (*V - k <= 0) {
				arg[i].val = 0;
                semop(semid, arg, 1);
				printf("Юнит %d вытащил %d золота, осталось %d\n", getpid(), *V, 0);
                exit(1);
            } else {
				*V = *V - k;
                *mine = *V;
                arg[i].val = 0;
                semop(semid, arg, 1);
                printf("Юнит %d вытащил %d золота, осталось %d\n", getpid(), k, *V);
			  }
            }

        }
    }
    int status;
	if (getpid() == pid_p ) {
		wait(&status);	
		printf("Шахта истощена\n");
        for(int j = 0; j < n; j++) {
			if( pid[j] != status && pid[j] != 0 ) 
				kill(pid[j], SIGKILL);										
				} 
		shmdt(mine);
        shmctl(shmid, IPC_RMID, 0);
        semctl(semid, 0, IPC_RMID, 0);
        printf("Семафоры и разделяемая память закрыты\n");
	}
return(1);
}
