#include <stdio.h>
#include <stdlib.h>

extern int f_pow(int n);
extern double f_sqrt(double n);

int main (int argc, char* argv[])
{
	if(argc < 3) {printf("error"); exit(-1);}
	
	int k = f_pow(atoi(argv[1]));
	double l = f_sqrt(atoi(argv[2]));
	printf("%s в квадрате равен %d\n", argv[1], k);
	printf("Корень из %s  равен %.4f\n", argv[2], l);
	
}
