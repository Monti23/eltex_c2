#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>



int main (int argc, char* argv[])
{
	if(argc < 3) {printf("error"); exit(-1);}
	void* ha = dlopen("lib_f.so", RTLD_LAZY);
	
	if(!ha)
		{ printf("Ошибка загрузки библиотеки"); exit(-1);
			}
	
	int (*func_pow)(int n);
	double (*func_sqrt)(double n);
	
	func_pow = dlsym(ha, "f_pow");
	func_sqrt = dlsym(ha, "f_sqrt");
	
	int n = atoi(argv[1]);
	int k = (func_pow)(n);
	n = (double)atoi(argv[2]);
	double l = (func_sqrt)(n);
	
	printf("%s в квадрате равен %d\n", argv[1], k);
	printf("Корень из %s  равен %.4f\n", argv[2], l);
	
	dlclose(ha);
}
