 #include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

int count =0;

int main(int argc, char *argv[]) {
	if (argc < 2) {
			printf("Usage: ./hunger_games int\n");
			exit(-1); 
		}		
	int n = atoi(argv[1]);
	pid_t pid[n];				// массив доч пидов
	memset(pid, 0, sizeof(int)*n);
	pid_t pid_p = getpid();
	int fd[n][2], fd_to_child[n][2];
	int read_buf = 0, l=0;
	
	for(int i = 0;i<n;i++) {
		pipe(fd_to_child[i]);
		pipe(fd[i]);
		pid[i] = fork();
		srand(getpid());
		
		if(pid[i] == -1) {
			perror("fork failed");
			exit(-1);
		}
		
		else if (pid[i] == 0) { 	// child process
			close(fd[i][0]);
			close(fd_to_child[i][1]);	
			count += i;
			break;
			}
		else {						
			close(fd[i][1]);
			close(fd_to_child[i][0]);					
		}
	}
	pid_t pid_win = 0;	
	
	if(getpid() == pid_p) {	
		for(int i=0; i<n; i++) {
			printf("Parents: Child's pid #%d = %d\n", i, pid[i]);
		}
		
		for(int j=0;j<n;j++) {
			for(int i=0;i<n;i++){
				write(fd_to_child[j][1], &pid[i], sizeof(int));	
			}}				// send pid's array to child
		for(int j=0;j<n-1;j++) {
			//read(fd[j][0], &read_buf, sizeof(int));
			read_buf = wait(NULL);
			for(int m=0; m<n; m++) {
				if(read_buf == pid[m]) pid[m] = 0;				
			}
			printf("parents: kill %d\n", read_buf);
			//pid[read_buf] = 0;
			if(j == n-2) {
				for(int m=0; m<n; m++){
					if(pid[m] != 0) {
						pid_win = pid[m];
						printf("child pid %d win\n", pid_win);
						kill(pid_win, SIGKILL);
					}}
			}
		}		
	}
		else {
			sleep(2);
			for(int i=0;i<n;i++){
				read(fd_to_child[count][0], &pid[i], sizeof(int));	
			}					// read pid's array
				
		while(l<n-1) {
			sleep(rand()%3);
			int k = rand()%n;				
			if(pid[k] != 0 && pid[k]!=getpid()) {
				kill(pid[k], SIGKILL);
				l++;
				//printf("child: kill %d\n", pid[k]);
				//write(fd[l][1], k, sizeof(int));
				pid[k] = 0;
			}
		}
		}
	exit(0);
}
